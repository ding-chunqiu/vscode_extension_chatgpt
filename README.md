# 环境配置

1. 安装依赖

```bash
yarn
```

2. 本地运行 键盘 F5

3. 打包成 VSIX 插件

```bash
npm i vsce -g

vsce package --no-dependencies
```
